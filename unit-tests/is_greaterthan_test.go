package unit_tests

import (
	"fmt"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_GreaterThan(t *testing.T) {
	// Arrange
	const actual = 10
	const expected = 5
	const expectedN = 15
	matcher := is.GreaterThan(expected)
	matcherN := is.GreaterThan(expectedN)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected "%d" (int) to be greater than "%d" (int)`, expectedN, actual)), "Message")
}

func Test_GreaterThanEqualTo(t *testing.T) {
	// Arrange
	const actual = 10
	const expected = 10
	const expectedN = 15
	matcher := is.GreaterThanOrEqualTo(expected)
	matcherN := is.GreaterThanOrEqualTo(expectedN)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected "%d" (int) to be greater than or equal to "%d" (int)`, expectedN, actual)), "Message")
}

func Test_LessThan(t *testing.T) {
	// Arrange
	const actual = 10
	const expected = 15
	const expectedN = 5
	matcher := is.LessThan(expected)
	matcherN := is.LessThan(expectedN)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected "%d" (int) to be less than "%d" (int)`, expectedN, actual)), "Message")
}

func Test_LessThanEqualTo(t *testing.T) {
	// Arrange
	const actual = 10
	const expected = 10
	const expectedN = 5
	matcher := is.LessThanOrEqualTo(expected)
	matcherN := is.LessThanOrEqualTo(expectedN)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected "%d" (int) to be less than or equal to "%d" (int)`, expectedN, actual)), "Message")
}
