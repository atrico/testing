package unit_tests

import (
	"fmt"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_DeepEqual_Equal(t *testing.T) {
	// Arrange
	actual := 0
	expected := 0
	matcher := is.DeepEqualTo(expected)
	matcherN := is.NotDeepEqualTo(expected)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%d" (int)`, actual)), "Not Message")
}

func Test_DeepEqual_NotEqual(t *testing.T) {
	// Arrange
	actual := 0
	expected := 1
	matcher := is.DeepEqualTo(expected)
	matcherN := is.NotDeepEqualTo(expected)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected "%d" (int), but found "%d" (int)`, expected, actual)), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}
