package unit_tests

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Kind_SameKindSlice(t *testing.T) {
	// Arrange
	actual := make([]int, 0)
	expected := reflect.TypeOf(make([]string, 0)).Kind()
	matcher := is.Kind(expected)
	matcherN := is.NotKind(expected)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected a kind other than "%v"`, expected)), "Not Message")
}

func Test_Kind_SameKindMap(t *testing.T) {
	// Arrange
	actual := make(map[string]int, 0)
	expected := reflect.TypeOf(make(map[int]string, 0)).Kind()
	matcher := is.Kind(expected)
	matcherN := is.NotKind(expected)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected a kind other than "%v"`, expected)), "Not Message")
}

func Test_Kind_DifferentKind(t *testing.T) {
	// Arrange
	actual := make([]int, 0)
	expected := reflect.TypeOf(make(map[int]string, 0)).Kind()
	matcher := is.Kind(expected)
	matcherN := is.NotKind(expected)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected kind "%v", but found "%T" (%v)`, expected, actual, actual)), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}
