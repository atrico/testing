package unit_tests

import (
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_True(t *testing.T) {
	// Arrange
	actual := true
	matcher := is.True
	matcherN := is.False

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.EqualTo(true), "Result")
	assert.That(t, resultN, is.EqualTo(false), "Not Result")
	assert.That(t, msgN, is.EqualTo(`Expected "false" (bool), but found "true" (bool)`), "Not Message")
}

func Test_False(t *testing.T) {
	// Arrange
	actual := false
	matcher := is.True
	matcherN := is.False

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.EqualTo(false), "Result")
	assert.That(t, msg, is.EqualTo(`Expected "true" (bool), but found "false" (bool)`), "Message")
	assert.That(t, resultN, is.EqualTo(true), "Not Result")
}
