package unit_tests

import (
	"fmt"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Equivalent_Equal(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3}
	expected := []int{1, 2, 3}
	matcher := is.EquivalentTo(expected)
	matcherN := is.NotEquivalentTo(expected)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%v"`, actual)), "Not Message")

}

func Test_Equivalent_WrongOrder(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3}
	expected := []int{2, 1, 3}
	matcher := is.EquivalentTo(expected)
	matcherN := is.NotEquivalentTo(expected)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%v"`, expected)), "Not Message")
}

func Test_Equivalent_CustomEquality_Equal(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3}
	expected := []int{2, 4, 6}
	customEquals := func(act, exp int) (bool, string) { return act == (exp / 2), "" }
	matcher := is.EquivalentToC(expected, customEquals)
	matcherN := is.NotEquivalentToC(expected, customEquals)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%v"`, expected)), "Not Message")

}

func Test_Equivalent_CustomEquality_WrongOrder(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3}
	expected := []int{4, 2, 6}
	customEquals := func(act, exp int) (bool, string) { return act == (exp / 2), "" }
	matcher := is.EquivalentToC(expected, customEquals)
	matcherN := is.NotEquivalentToC(expected, customEquals)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%v"`, expected)), "Not Message")
}

func Test_Equivalent_Duplicate(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3, 1}
	expected := []int{2, 1, 3, 1}
	matcher := is.EquivalentTo(expected)
	matcherN := is.NotEquivalentTo(expected)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%v"`, expected)), "Not Message")
}

func Test_Equivalent_ExtraItems(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3, 4, 5}
	expected := []int{1, 2, 3}
	matcher := is.EquivalentTo(expected)
	matcherN := is.NotEquivalentTo(expected)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected (equivalent to) "%d", but found "%d", Extra items "%v"`, expected, actual, []int{4, 5})), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}

func Test_Equivalent_MissingItems(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3}
	expected := []int{1, 2, 3, 4, 5}
	matcher := is.EquivalentTo(expected)
	matcherN := is.NotEquivalentTo(expected)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected (equivalent to) "%d", but found "%d", Missing items "%v"`, expected, actual, []int{4, 5})), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}

func Test_Equivalent_ExtraAndMissingItems(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3, 4, 5}
	expected := []int{1, 2, 3, 6, 7}
	matcher := is.EquivalentTo(expected)
	matcherN := is.NotEquivalentTo(expected)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected (equivalent to) "%d", but found "%d", Extra items "%v", Missing items "%v"`, expected, actual, []int{4, 5}, []int{6, 7})), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}

func Test_Equivalent_DuplicateExtraAndMissingItems(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3, 4, 4}
	expected := []int{1, 2, 3, 3, 4}
	matcher := is.EquivalentTo(expected)
	matcherN := is.NotEquivalentTo(expected)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected (equivalent to) "%d", but found "%d", Extra items "%v", Missing items "%v"`, expected, actual, []int{4}, []int{3})), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}

func Test_Equivalent_CustomEquality_DuplicateExtraAndMissingItems(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3, 4, 4}
	expected := []int{2, 4, 6, 6, 8}
	customEquals := func(act, exp int) (bool, string) { return act == (exp / 2), "" }
	matcher := is.EquivalentToC(expected, customEquals)
	matcherN := is.NotEquivalentToC(expected, customEquals)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected (equivalent to) "%d", but found "%d", Extra items "%v", Missing items "%v"`, expected, actual, []int{4}, []int{6})), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}
