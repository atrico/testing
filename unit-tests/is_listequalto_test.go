package unit_tests

import (
	"fmt"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_ListEqual_Equal(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3, 4}
	expected := []int{1, 2, 3, 4}
	matcher := is.ListEqualTo(expected)
	matcherN := is.NotListEqualTo(expected)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%v" (%T)`, actual, actual)), "Not Message")
}

func Test_ListEqual_NotEqual(t *testing.T) {
	// Arrange
	actual := []float64{1, 2, 3}
	expected := []float64{1, 2}
	matcher := is.ListEqualTo(expected)
	matcherN := is.NotListEqualTo(expected)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected "%v" (%T), but found "%v" (%T)`, expected, expected, actual, actual)), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}

func Test_ListEqual_CustomEquality_Equal(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3, 4}
	expected := []int{2, 4, 6, 8}
	customMsg := rg.String()
	customEquals := func(act, exp int) (bool, string) { return act == (exp / 2), customMsg }
	matcher := is.ListEqualToC(expected, customEquals)
	matcherN := is.NotListEqualToC(expected, customEquals)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%v" (%T)`, expected, expected)), "Not Message (no custom added)")
}

func Test_ListEqual_CustomEquality_NotEqual(t *testing.T) {
	// Arrange
	actual := []int{1, 2, 3, 4}
	expected := []int{4, 4, 6, 8}
	customMsg := rg.String()
	customEquals := func(act, exp int) (bool, string) { return act == (exp / 2), customMsg }
	matcher := is.ListEqualToC(expected, customEquals)
	matcherN := is.NotListEqualToC(expected, customEquals)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected "%v" (%T), but found "%v" (%T) (%s)`, expected, expected, actual, actual, customMsg)), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}

func Test_ListEqual_NilEqual(t *testing.T) {
	// Arrange
	actual := []int(nil)
	expected := []int{}
	matcher := is.ListEqualTo(expected)
	matcherN := is.NotListEqualTo(expected)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%v" (%T)`, actual, actual)), "Not Message")
}

func Test_ListEqual_NilNotEqual(t *testing.T) {
	// Arrange
	actual := []float64(nil)
	expected := []float64{1, 2, 3}
	matcher := is.ListEqualTo(expected)
	matcherN := is.NotListEqualTo(expected)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected "%v" (%T), but found "%v" (%T)`, expected, expected, actual, actual)), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}

func Test_ListEqual_CustomEquality_NilEqual(t *testing.T) {
	// Arrange
	actual := []int(nil)
	expected := []int{}
	customMsg := rg.String()
	customEquals := func(act, exp int) (bool, string) { return act == (exp / 2), customMsg }
	matcher := is.ListEqualToC(expected, customEquals)
	matcherN := is.NotListEqualToC(expected, customEquals)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%v" (%T)`, expected, expected)), "Not Message (no custom added)")
}

func Test_ListEqual_CustomEquality_NilNotEqual(t *testing.T) {
	// Arrange
	actual := []int(nil)
	expected := []int{1, 2, 3, 4}
	customMsg := rg.String()
	customEquals := func(act, exp int) (bool, string) { return act == (exp / 2), customMsg }
	matcher := is.ListEqualToC(expected, customEquals)
	matcherN := is.NotListEqualToC(expected, customEquals)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected "%v" (%T), but found "%v" (%T)`, expected, expected, actual, actual)), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}
