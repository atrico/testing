package unit_tests

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

type TestType2 struct{}

func Test_Type_SameType(t *testing.T) {
	// Arrange
	actual := TestType{}
	expected := reflect.TypeOf(TestType{})
	matcher := is.Type(expected)
	matcherN := is.NotType(expected)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected a type other than "%v"`, expected)), "Not Message")
}

func Test_Type_DifferentType(t *testing.T) {
	// Arrange
	actual := TestType2{}
	expected := reflect.TypeOf(TestType{})
	matcher := is.Type(expected)
	matcherN := is.NotType(expected)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected type "%v", but found "%T" (%v)`, expected, actual, actual)), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}
