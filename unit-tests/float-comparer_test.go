package unit_tests

import (
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/equality"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Float64Comparer_Equal(t *testing.T) {
	// Arrange
	lhs := rg.Float64()
	rhs := lhs
	comparer := equality.Float64(6)

	// Act
	resultLL, _ := comparer(lhs, lhs)
	resultRR, _ := comparer(rhs, rhs)
	resultLR, _ := comparer(lhs, rhs)
	resultRL, _ := comparer(rhs, lhs)

	// Assert
	assert.That(t, resultLL, is.True, "LL")
	assert.That(t, resultRR, is.True, "RR")
	assert.That(t, resultLR, is.True, "LR")
	assert.That(t, resultRL, is.True, "RL")
}

func Test_Float64Comparer_NotEqual(t *testing.T) {
	// Arrange
	lhs := rg.Float64()
	rhs := lhs + 0.001
	comparer := equality.Float64(3)

	// Act
	resultLL, _ := comparer(lhs, lhs)
	resultRR, _ := comparer(rhs, rhs)
	resultLR, _ := comparer(lhs, rhs)
	resultRL, _ := comparer(rhs, lhs)

	// Assert
	assert.That(t, resultLL, is.True, "LL")
	assert.That(t, resultRR, is.True, "RR")
	assert.That(t, resultLR, is.False, "LR")
	assert.That(t, resultRL, is.False, "RL")
}

func Test_Float64Comparer_EqualEnough(t *testing.T) {
	// Arrange
	lhs := rg.Float64()
	rhs := lhs + 0.0001
	comparer := equality.Float64(3)

	// Act
	resultLL, _ := comparer(lhs, lhs)
	resultRR, _ := comparer(rhs, rhs)
	resultLR, _ := comparer(lhs, rhs)
	resultRL, _ := comparer(rhs, lhs)

	// Assert
	assert.That(t, resultLL, is.True, "LL")
	assert.That(t, resultRR, is.True, "RR")
	assert.That(t, resultLR, is.True, "LR")
	assert.That(t, resultRL, is.True, "RL")
}

func Test_Float32Comparer_Equal(t *testing.T) {
	// Arrange
	lhs := rg.Float32()
	rhs := lhs
	comparer := equality.Float32(6)

	// Act
	resultLL, _ := comparer(lhs, lhs)
	resultRR, _ := comparer(rhs, rhs)
	resultLR, _ := comparer(lhs, rhs)
	resultRL, _ := comparer(rhs, lhs)

	// Assert
	assert.That(t, resultLL, is.True, "LL")
	assert.That(t, resultRR, is.True, "RR")
	assert.That(t, resultLR, is.True, "LR")
	assert.That(t, resultRL, is.True, "RL")
}

func Test_Float32Comparer_NotEqual(t *testing.T) {
	// Arrange
	lhs := rg.Float32()
	rhs := lhs + 0.001
	comparer := equality.Float32(3)

	// Act
	resultLL, _ := comparer(lhs, lhs)
	resultRR, _ := comparer(rhs, rhs)
	resultLR, _ := comparer(lhs, rhs)
	resultRL, _ := comparer(rhs, lhs)

	// Assert
	assert.That(t, resultLL, is.True, "LL")
	assert.That(t, resultRR, is.True, "RR")
	assert.That(t, resultLR, is.False, "LR")
	assert.That(t, resultRL, is.False, "RL")
}

func Test_Float32Comparer_EqualEnough(t *testing.T) {
	// Arrange
	lhs := rg.Float32()
	rhs := lhs + 0.0001
	comparer := equality.Float32(3)
	t.Logf("%v, %v", lhs, rhs)

	// Act
	resultLL, _ := comparer(lhs, lhs)
	resultRR, _ := comparer(rhs, rhs)
	resultLR, _ := comparer(lhs, rhs)
	resultRL, _ := comparer(rhs, lhs)

	// Assert
	assert.That(t, resultLL, is.True, "LL")
	assert.That(t, resultRR, is.True, "RR")
	assert.That(t, resultLR, is.True, "LR")
	assert.That(t, resultRL, is.True, "RL")
}

func Test_Float32Comparer_CornerCase1(t *testing.T) {
	// Arrange
	lhs := float32(0.44492538)
	rhs := float32(0.44502538) // 3rd place clocks over
	comparer := equality.Float32(3)
	t.Logf("%v, %v", lhs, rhs)

	// Act
	resultLL, _ := comparer(lhs, lhs)
	resultRR, _ := comparer(rhs, rhs)
	resultLR, _ := comparer(lhs, rhs)
	resultRL, _ := comparer(rhs, lhs)

	// Assert
	assert.That(t, resultLL, is.True, "LL")
	assert.That(t, resultRR, is.True, "RR")
	assert.That(t, resultLR, is.True, "LR")
	assert.That(t, resultRL, is.True, "RL")
}
