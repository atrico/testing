package unit_tests

import (
	"fmt"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Equal_Equal(t *testing.T) {
	// Arrange
	actual := 0
	expected := 0
	matcher := is.EqualTo(expected)
	matcherN := is.NotEqualTo(expected)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%d" (int)`, actual)), "Not Message")
}

func Test_Equal_NotEqual(t *testing.T) {
	// Arrange
	actual := 0
	expected := 1
	matcher := is.EqualTo(expected)
	matcherN := is.NotEqualTo(expected)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected "%d" (int), but found "%d" (int)`, expected, actual)), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}

func Test_Equal_CustomEquality_Equal(t *testing.T) {
	// Arrange
	actual := 1
	expected := 2
	customMsg := rg.String()
	customEquals := func(act, exp int) (bool, string) { return act == (exp / 2), customMsg }
	matcher := is.EqualToC(expected, customEquals)
	matcherN := is.NotEqualToC(expected, customEquals)

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(fmt.Sprintf(`Expected something other than "%d" (int)`, expected)), "Not Message (no custom added)")
}

func Test_Equal_CustomEquality_NotEqual(t *testing.T) {
	// Arrange
	actual := 1
	expected := 4
	customMsg := rg.String()
	customEquals := func(act, exp int) (bool, string) { return act == (exp / 2), customMsg }
	matcher := is.EqualToC(expected, customEquals)
	matcherN := is.NotEqualToC(expected, customEquals)

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected "%d" (int), but found "%d" (int) (%s)`, expected, actual, customMsg)), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}
