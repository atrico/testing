package unit_tests

import (
	"fmt"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Nil_Nil(t *testing.T) {
	// Arrange
	var actual any = nil
	matcher := is.Nil
	matcherN := is.NotNil

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(`Expected something other than "nil"`), "Not Message")
}

func Test_Nil_NotNil(t *testing.T) {
	// Arrange
	actual := TestType{}
	matcher := is.Nil
	matcherN := is.NotNil

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected "nil", but found "%v" (%T)`, actual, actual)), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}

func Test_PtrNil_Nil(t *testing.T) {
	// Arrange
	var actual *string = nil
	matcher := is.PtrNil[string]()
	matcherN := is.NotPtrNil[string]()

	// Act
	result, _ := matcher(actual)
	resultN, msgN := matcherN(actual)

	// Assert
	assert.That(t, result, is.True, "Result")
	assert.That(t, resultN, is.False, "Not Result")
	assert.That(t, msgN, is.EqualTo(`Expected something other than "<nil>" (*string)`), "Not Message")
}

func Test_PtrNil_NotNil(t *testing.T) {
	// Arrange
	actual := &TestType{}
	matcher := is.PtrNil[TestType]()
	matcherN := is.NotPtrNil[TestType]()

	// Act
	result, msg := matcher(actual)
	resultN, _ := matcherN(actual)

	// Assert
	assert.That(t, result, is.False, "Result")
	assert.That(t, msg, is.EqualTo(fmt.Sprintf(`Expected "<nil>" (%T), but found "%v" (%T)`, actual, actual, actual)), "Message")
	assert.That(t, resultN, is.True, "Not Result")
}
