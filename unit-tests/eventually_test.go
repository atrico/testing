package unit_tests

import (
	"context"
	"testing"
	"time"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

var timeout = 500 * time.Millisecond
var halfTimeout = 250 * time.Millisecond

func Test_Eventually(t *testing.T) {
	// Arrange
	signal := false

	// Act
	go func() { time.Sleep(halfTimeout); signal = true }()

	// Assert
	assert.That(t, signal, is.False, "initially false")
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	assert.ThatEventually(t, ctx, func() bool { return signal }, is.True, "eventually true")
}
