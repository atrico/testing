package testing

func PanicCatcher(action func()) (thePanic any) {
	func() {
		defer func() { thePanic = recover() }()
		action()
	}()
	return thePanic
}
