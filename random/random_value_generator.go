package random

import (
	"errors"
	"fmt"
	"math/rand"
	"reflect"
	"time"
)

type ValueGenerator interface {
	String() string
	StringOfLen(length int) string
	Identifier() string
	IdentifierOfLen(length int) string
	Rune() rune
	Byte() byte
	Int() int
	IntUpto(max int) int
	IntBetween(min, max int) int
	Int64() int64
	Uint() uint
	Float32() float32
	Float64() float64
	Complex64() complex64
	Complex128() complex128
	Bool() bool
	Time() time.Time
	Duration() time.Duration

	// Get a value of any (supported) type
	Value(receiver any) error
}

type ValueGeneratorModifier interface {
	ValueGenerator
	SetDefaultStringLength(length int) ValueGeneratorModifier
	SetDefaultSliceLength(length int) ValueGeneratorModifier
	SetDefaultIntNormal() ValueGeneratorModifier
	SetDefaultIntUpto(max int) ValueGeneratorModifier
	SetDefaultIntBetween(min, max int) ValueGeneratorModifier
}

func NewValueGenerator() ValueGeneratorModifier {
	return &randomValueGenerator{
		rn:                  rand.New(rand.NewSource(time.Now().UnixNano())),
		defaultStringLength: 5,
		defaultSliceLength:  3,
		defaultIntType:      IntNormal,
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type IntType byte

const (
	IntNormal  IntType = iota
	IntUpto    IntType = iota
	IntBetween IntType = iota
)

type randomValueGenerator struct {
	rn                  *rand.Rand
	defaultStringLength int
	defaultSliceLength  int
	defaultIntType      IntType
	defaultMinInt       int
	defaultMaxInt       int
}

var allChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
var idChars = "abcdefghijklmnopqrstuvwxyz"

func (r *randomValueGenerator) String() string {
	return r.StringOfLen(r.defaultStringLength)
}

func (r *randomValueGenerator) StringOfLen(length int) string {
	return stringOfLen(allChars, length)
}
func (r *randomValueGenerator) Identifier() string {
	return r.IdentifierOfLen(r.defaultStringLength)
}

func (r *randomValueGenerator) IdentifierOfLen(length int) string {
	return stringOfLen(idChars, length)
}

func stringOfLen(chars string, length int) string {
	text := make([]byte, length)
	for i := range text {
		n := rand.Intn(len(chars))
		text[i] = chars[n]
	}
	return string(text)
}

func (r *randomValueGenerator) Byte() byte {
	return byte(r.rn.Int())
}

func (r *randomValueGenerator) Int() int {
	switch r.defaultIntType {
	case IntNormal:
		return r.rn.Int()
	case IntUpto:
		return r.IntUpto(r.defaultMaxInt)
	case IntBetween:
		return r.IntBetween(r.defaultMinInt, r.defaultMaxInt)
	default:
		panic(errors.New("invalid default int type"))
	}
}

func (r *randomValueGenerator) IntUpto(max int) int {
	return r.rn.Intn(max)
}

func (r *randomValueGenerator) IntBetween(min, max int) int {
	return r.rn.Intn(max-min) + min
}

func (r *randomValueGenerator) Int64() int64 {
	return r.rn.Int63()
}

func (r *randomValueGenerator) Rune() rune {
	return rune(r.rn.Uint32())
}

func (r *randomValueGenerator) Uint() uint {
	return uint(r.rn.Uint32())
}

func (r *randomValueGenerator) Float32() float32 {
	return r.rn.Float32()
}

func (r *randomValueGenerator) Float64() float64 {
	return r.rn.Float64()
}

func (r *randomValueGenerator) Complex64() complex64 {
	return complex(r.rn.Float32(), r.rn.Float32())
}

func (r *randomValueGenerator) Complex128() complex128 {
	return complex(r.rn.Float64(), r.rn.Float64())
}

func (r *randomValueGenerator) Bool() bool {
	return r.rn.Intn(2) == 0
}

func (r *randomValueGenerator) Time() time.Time {
	return time.Unix(0, r.Int64())
}

func (r *randomValueGenerator) Duration() time.Duration {
	return time.Duration(r.Int64())
}

func (r *randomValueGenerator) Value(receiver any) (err error) {
	rValue := reflect.ValueOf(receiver)
	// Must be a pointer
	if rValue.Kind() == reflect.Ptr {
		var val any
		if val, err = r.createValue(rValue.Type().Elem()); err == nil {
			rValue.Elem().Set(reflect.ValueOf(val))
		}
	} else {
		err = errors.New("receiver is not a pointer")
	}
	return
}

func (r *randomValueGenerator) createValue(rType reflect.Type) (value any, err error) {
	var val any
	// Check types
	if rType.PkgPath() == "time" && rType.Name() == "Time" {
		value = r.Time()
	} else if rType.PkgPath() == "time" && rType.Name() == "Duration" {
		value = r.Duration()
	} else {
		// Check kind
		switch rType.Kind() {
		case reflect.String:
			value = r.String()
		case reflect.Int:
			value = r.Int()
		case reflect.Int8:
			value = int8(r.Int())
		case reflect.Int16:
			value = int16(r.Int())
		case reflect.Int32:
			value = int32(r.Int())
		case reflect.Int64:
			value = r.Int64()
		case reflect.Uint:
			value = r.Uint()
		case reflect.Uint8:
			value = uint8(r.Uint())
		case reflect.Uint16:
			value = uint16(r.Uint())
		case reflect.Uint32:
			value = r.rn.Uint32()
		case reflect.Uint64:
			value = r.rn.Uint64()
		case reflect.Float32:
			value = r.Float32()
		case reflect.Float64:
			value = r.Float64()
		case reflect.Complex64:
			value = r.Complex64()
		case reflect.Complex128:
			value = r.Complex128()
		case reflect.Bool:
			value = r.Bool()
		case reflect.Ptr:
			vVal := reflect.New(rType.Elem())
			value = vVal.Interface()
			if val, err = r.createValue(rType.Elem()); err == nil {
				vVal.Elem().Set(reflect.ValueOf(val))
			}
		case reflect.Slice:
			vVal := reflect.MakeSlice(rType, r.defaultSliceLength, r.defaultSliceLength)
			value = vVal.Interface()
			eType := rType.Elem()
			for i := 0; i < r.defaultSliceLength; i++ {
				if val, err = r.createValue(eType); err == nil {
					vVal.Index(i).Set(reflect.ValueOf(val))
				}
			}
		case reflect.Map:
			vVal := reflect.MakeMap(rType)
			value = vVal.Interface()
			tKey := rType.Key()
			tElem := rType.Elem()
			vVal.SetMapIndex(reflect.ValueOf("str"), reflect.ValueOf(123))
			for i := 0; i < r.defaultSliceLength; i++ {
				if kVal, kErr := r.createValue(tKey); kErr == nil {
					if mVal, mErr := r.createValue(tElem); mErr == nil {
						vVal.SetMapIndex(reflect.ValueOf(kVal), reflect.ValueOf(mVal))
					} else {
						return value, mErr
					}
				} else {
					return value, kErr
				}
			}
		case reflect.Struct:
			vVal := reflect.New(rType).Elem()
			value = vVal.Interface()
			for i := 0; i < vVal.NumField(); i++ {
				vField := vVal.Field(i)
				if val, err = r.createValue(vField.Type()); err == nil {
					vField.Set(reflect.ValueOf(val))
				}
			}
		default:
			err = errors.New(fmt.Sprintf("unsupported type: %v", rType.Kind()))
		}
	}
	return
}

func (r *randomValueGenerator) SetDefaultStringLength(value int) ValueGeneratorModifier {
	r.defaultStringLength = value
	return r
}

func (r *randomValueGenerator) SetDefaultSliceLength(value int) ValueGeneratorModifier {
	r.defaultSliceLength = value
	return r
}

func (r *randomValueGenerator) SetDefaultIntNormal() ValueGeneratorModifier {
	r.defaultIntType = IntNormal
	return r
}

func (r *randomValueGenerator) SetDefaultIntUpto(max int) ValueGeneratorModifier {
	r.defaultMaxInt = max
	r.defaultIntType = IntUpto
	return r
}

func (r *randomValueGenerator) SetDefaultIntBetween(min, max int) ValueGeneratorModifier {
	r.defaultMinInt = min
	r.defaultMaxInt = max
	r.defaultIntType = IntBetween
	return r
}
