package equality

type Comparer[T any] func(act, exp T) (equal bool, msg string)
