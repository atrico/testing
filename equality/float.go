package equality

import (
	"math"
)

func Float64(places int) Comparer[float64] {
	return func(act, exp float64) (equal bool, msg string) {
		return compareFloatsImpl(act, exp, places)
	}
}

func Float32(places int) Comparer[float32] {
	return func(act, exp float32) (equal bool, msg string) {
		return compareFloatsImpl(float64(act), float64(exp), places)
	}
}

func Complex128(places int) Comparer[complex128] {
	return func(act, exp complex128) (equal bool, msg string) {
		return compareFloatsImpl(real(act), real(exp), places)
	}
}

func Complex64(places int) Comparer[complex64] {
	return func(act, exp complex64) (equal bool, msg string) {
		return compareFloatsImpl(float64(real(act)), float64(real(exp)), places)
	}
}

func compareFloatsImpl(act, exp float64, places int) (equal bool, msg string) {
	scale := math.Pow(10, float64(places))
	actI := int64(math.Round(act * scale))
	expI := int64(math.Round(exp * scale))
	return actI == expI, ""
}
