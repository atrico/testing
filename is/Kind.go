package is

import (
	"reflect"

	. "gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/messages"
)

func Kind(expected reflect.Kind) Matcher[any] {
	return CreateMatcher[any](kindMatch(expected), messages.ExpectedKindButActual[any](expected))
}

func NotKind(expected reflect.Kind) Matcher[any] {
	return CreateNotMatcher[any](kindMatch(expected), messages.ExpectedKindOtherThan[any](expected))
}

func kindMatch(expected reflect.Kind) MatcherImplementation[any] {
	return func(actual any) (match bool, message string) { return reflect.TypeOf(actual).Kind() == expected, "" }
}
