package is

import (
	. "gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/messages"
)

var True = CreateMatcher[bool](equalsMatch(true), messages.ExpectedButActual(true))

var False = CreateMatcher[bool](equalsMatch(false), messages.ExpectedButActual(false))
