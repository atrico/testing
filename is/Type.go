package is

import (
	"reflect"

	. "gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/messages"
)

func Type(expected reflect.Type) Matcher[any] {
	return CreateMatcher[any](typeMatch(expected), messages.ExpectedTypeButActual[any](expected))
}

func NotType(expected reflect.Type) Matcher[any] {
	return CreateNotMatcher[any](typeMatch(expected), messages.ExpectedTypeOtherThan[any](expected))
}

func typeMatch(expected reflect.Type) MatcherImplementation[any] {
	return func(actual any) (match bool, message string) { return reflect.TypeOf(actual) == expected, "" }
}
