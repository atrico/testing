package is

import (
	"reflect"

	. "gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/equality"
	"gitlab.com/atrico/testing/v2/messages"
)

func EqualTo[T any](expected T) Matcher[T] {
	return EqualToC(expected, defaultComparer[T]())
}

func NotEqualTo[T any](expected T) Matcher[T] {
	return NotEqualToC(expected, defaultComparer[T]())
}

func EqualToC[T any](expected T, comparer equality.Comparer[T]) Matcher[T] {
	return CreateMatcher(equalsMatchC(expected, comparer), messages.ExpectedButActual(expected))
}

func NotEqualToC[T any](expected T, comparer equality.Comparer[T]) Matcher[T] {
	return CreateNotMatcher(equalsMatchC(expected, comparer), messages.ExpectedOtherThan(expected))
}

func equalsMatch[T any](expected T) MatcherImplementation[T] {
	return equalsMatchC(expected, defaultComparer[T]())
}

func equalsMatchC[T any](expected T, comparer equality.Comparer[T]) MatcherImplementation[T] {
	return func(actual T) (match bool, message string) { return comparer(actual, expected) }
}

func defaultComparer[T any]() equality.Comparer[T] {
	return func(act, exp T) (bool, string) { return reflect.DeepEqual(act, exp), "" }
}
