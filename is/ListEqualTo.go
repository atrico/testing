package is

import (
	. "gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/equality"
	"gitlab.com/atrico/testing/v2/messages"
)

func ListEqualTo[T any](expected []T) Matcher[[]T] {
	return ListEqualToC(expected, defaultComparer[T]())
}

func NotListEqualTo[T any](expected []T) Matcher[[]T] {
	return NotListEqualToC(expected, defaultComparer[T]())
}

func ListEqualToC[T any](expected []T, comparer equality.Comparer[T]) Matcher[[]T] {
	return CreateMatcher(listEqualsMatchC(expected, comparer), messages.ExpectedButActual(expected))
}

func NotListEqualToC[T any](expected []T, comparer equality.Comparer[T]) Matcher[[]T] {
	return CreateNotMatcher(listEqualsMatchC(expected, comparer), messages.ExpectedOtherThan(expected))
}

func listEqualsMatchC[T any](expected []T, comparer equality.Comparer[T]) MatcherImplementation[[]T] {
	return func(actual []T) (match bool, message string) {
		if len(actual) == len(expected) {
			match = true
			for i, act := range actual {
				if match, message = comparer(act, expected[i]); !match {
					break
				}
			}
		}
		return match, message
	}
}
