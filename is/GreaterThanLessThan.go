package is

import (
	. "gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/messages"
	"golang.org/x/exp/constraints"
)

type Numeric interface {
	constraints.Integer | constraints.Float
}

func GreaterThan[T Numeric](expected T) Matcher[T] {
	return CreateMatcher(greaterThanMatch(expected), messages.ExpectedOperatorActual(expected, " to be greater than "))
}

func GreaterThanOrEqualTo[T Numeric](expected T) Matcher[T] {
	return CreateNotMatcher(lessThanMatch(expected), messages.ExpectedOperatorActual(expected, " to be greater than or equal to "))
}

func LessThan[T Numeric](expected T) Matcher[T] {
	return CreateMatcher(lessThanMatch(expected), messages.ExpectedOperatorActual(expected, " to be less than "))
}

func LessThanOrEqualTo[T Numeric](expected T) Matcher[T] {
	return CreateNotMatcher(greaterThanMatch(expected), messages.ExpectedOperatorActual(expected, " to be less than or equal to "))
}

func greaterThanMatch[T Numeric](expected T) MatcherImplementation[T] {
	return func(actual T) (match bool, message string) { return actual > expected, "" }
}
func lessThanMatch[T Numeric](expected T) MatcherImplementation[T] {
	return func(actual T) (match bool, message string) { return actual < expected, "" }
}
