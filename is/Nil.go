package is

import (
	. "gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/messages"
)

var Nil = CreateMatcher[any](nilMatch, messages.ExpectedButActual[any](nil))

var NotNil = CreateNotMatcher[any](nilMatch, messages.ExpectedOtherThan[any](nil))

var nilMatch = equalsMatch[any](nil)

func PtrNil[T any]() Matcher[*T] {
	return CreateMatcher[*T](nilPtrMatch[T](), messages.ExpectedButActual[*T](nil))
}

func NotPtrNil[T any]() Matcher[*T] {
	return CreateNotMatcher[*T](nilPtrMatch[T](), messages.ExpectedOtherThan[*T](nil))
}

func nilPtrMatch[T any]() MatcherImplementation[*T] {
	return equalsMatch[*T](nil)
}
