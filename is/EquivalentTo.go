package is

import (
	"fmt"
	"sort"
	"strings"

	. "gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/equality"
	"gitlab.com/atrico/testing/v2/messages"
)

func EquivalentTo[T any](expected []T) Matcher[[]T] {
	return EquivalentToC(expected, defaultComparer[T]())
}

func NotEquivalentTo[T any](expected []T) Matcher[[]T] {
	return NotEquivalentToC(expected, defaultComparer[T]())
}

func EquivalentToC[T any](expected []T, comparer equality.Comparer[T]) Matcher[[]T] {
	return func(actual []T) (bool, string) {
		return equivalentToMatcher(actual, expected, comparer)
	}
}

func NotEquivalentToC[T any](expected []T, comparer equality.Comparer[T]) Matcher[[]T] {
	return func(actual []T) (bool, string) {
		ok, _ := equivalentToMatcher(actual, expected, comparer)
		if !ok {
			return true, ""
		}
		return false, messages.ExpectedOtherThanNoType(expected)(actual, "")
	}
}

func equivalentToMatcher[T any](actual, expected []T, comparer equality.Comparer[T]) (bool, string) {
	extra := makeIndexSet(actual)
	missing := makeIndexSet(expected)
	for actIdx, actItem := range actual {
		for expIdx := range missing {
			if ok, _ := comparer(actItem, expected[expIdx]); ok {
				delete(extra, actIdx)
				delete(missing, expIdx)
				break
			}
		}
	}
	// Success
	if len(extra) == 0 && len(missing) == 0 {
		return true, ""
	}
	msg := strings.Builder{}
	msg.WriteString(fmt.Sprintf(`Expected (equivalent to) "%v", but found "%v"`, expected, actual))
	if len(extra) > 0 {
		msg.WriteString(fmt.Sprintf(`, Extra items "%v"`, mapIndicesToValues(extra, actual)))
	}
	if len(missing) > 0 {
		msg.WriteString(fmt.Sprintf(`, Missing items "%v"`, mapIndicesToValues(missing, expected)))
	}
	return false, msg.String()

}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type intSet map[int]any

func makeIndexSet[T any](slc []T) intSet {
	out := make(intSet, len(slc))
	for idx := range slc {
		out[idx] = nil
	}
	return out
}

func mapIndicesToValues[T any](indices intSet, slc []T) []T {
	idxSlice := make([]int, len(indices))
	i := 0
	for k := range indices {
		idxSlice[i] = k
		i++
	}
	sort.Slice(idxSlice, func(i, j int) bool { return idxSlice[i] < idxSlice[j] })
	out := make([]T, len(idxSlice))
	for i, v := range idxSlice {
		out[i] = slc[v]
	}
	return out
}
