package is

import (
	"reflect"

	. "gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/messages"
)

func DeepEqualTo[T any](expected T) Matcher[T] {
	return CreateMatcher(deepEqualsMatch(expected), messages.ExpectedButActual(expected))
}

func NotDeepEqualTo[T any](expected T) Matcher[T] {
	return CreateNotMatcher(deepEqualsMatch(expected), messages.ExpectedOtherThan(expected))
}

func deepEqualsMatch[T any](expected T) MatcherImplementation[T] {
	return func(actual T) (match bool, message string) { return reflect.DeepEqual(actual, expected), "" }
}
