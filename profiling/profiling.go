package profiling

import (
	"os"
	"runtime/pprof"
)

type Config func(cfg *config)

// Start CPU profiling
func Start(mods ...Config) (stop func(), err error) {
	cfg := doConfig(mods, defaultCpuFileName)
	stop = func() {}
	var file *os.File
	if file, err = os.Create(cfg.file); err == nil {
		if err = pprof.StartCPUProfile(file); err == nil {
			stop = pprof.StopCPUProfile
		}
	}
	return
}

// Write heap data
func WriteHeap(mods ...Config) (err error) {
	cfg := doConfig(mods, defaultHeapFileName)
	var file *os.File
	if file, err = os.Create(cfg.file); err == nil {
		err = pprof.WriteHeapProfile(file)
	}
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// Config
// ----------------------------------------------------------------------------------------------------------------------------

func File(name string) Config {
	return func(cfg *config) {
		cfg.file = name
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Config Implementation
// ----------------------------------------------------------------------------------------------------------------------------
const defaultCpuFileName = "cpu.prof"
const defaultHeapFileName = "heap.prof"

type config struct {
	file string
}

func doConfig(mods []Config, defaultName string) (cfg config) {
	cfg.file = defaultName
	for _, m := range mods {
		m(&cfg)
	}
	return
}
