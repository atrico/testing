package messages

import (
	"fmt"
	"reflect"

	"gitlab.com/atrico/testing/v2/assert"
)

func ExpectedOperatorActual[T any](expected T, operator string) assert.MessageProvider[T] {
	if any(expected) == nil {
		return func(actual T, extra string) string {
			return formatWithExtra(extra, `Expected "nil"%s"%v" (%T)`, operator, actual, actual)
		}
	}
	return func(actual T, extra string) string {
		return formatWithExtra(extra, `Expected "%v" (%T)%s"%v" (%T)`, expected, expected, operator, actual, actual)
	}
}

func ExpectedButActual[T any](expected T) assert.MessageProvider[T] {
	return ExpectedOperatorActual(expected, ", but found ")
}

func ExpectedTypeButActual[T any](expected reflect.Type) assert.MessageProvider[any] {
	return func(actual any, extra string) string {
		return formatWithExtra(extra, `Expected type "%v", but found "%T" (%v)`, expected, actual, actual)
	}
}

func ExpectedKindButActual[T any](expected reflect.Kind) assert.MessageProvider[any] {
	return func(actual any, extra string) string {
		return formatWithExtra(extra, `Expected kind "%v", but found "%T" (%v)`, expected, actual, actual)
	}
}

func ExpectedOtherThan[T any](expected T) assert.MessageProvider[T] {
	if any(expected) == nil {
		return func(actual T, extra string) string { return `Expected something other than "nil"` }
	}
	return func(actual T, extra string) string {
		return formatWithExtra(extra, `Expected something other than "%v" (%T)`, expected, expected)
	}
}
func ExpectedOtherThanNoType[T any](expected T) assert.MessageProvider[T] {
	if any(expected) == nil {
		return func(actual T, extra string) string { return `Expected something other than "nil"` }
	}
	return func(actual T, extra string) string {
		return formatWithExtra(extra, `Expected something other than "%v"`, expected)
	}
}

func ExpectedTypeOtherThan[T any](expected reflect.Type) assert.MessageProvider[any] {
	return func(actual any, extra string) string {
		return formatWithExtra(extra, `Expected a type other than "%v"`, expected)
	}
}

func ExpectedKindOtherThan[T any](expected reflect.Kind) assert.MessageProvider[any] {
	return func(actual any, extra string) string {
		return formatWithExtra(extra, `Expected a kind other than "%v"`, expected)
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

func formatWithExtra(extra string, format string, args ...any) string {
	if extra != "" {
		return fmt.Sprintf(format+" (%s)", append(args, extra)...)
	}
	return fmt.Sprintf(format, args...)
}
