package assert

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func That[T any](t *testing.T, actual T, matcher Matcher[T], reasonFormat string, reasonArgs ...any) {
	pass, message := matcher(actual)
	if !pass {
		output := fmt.Sprintf(reasonFormat, reasonArgs...)
		sep := ""
		if len(output) > 0 {
			sep = " => "
		}
		Fail(t, "%s%s%s", output, sep, message)
	}
}

func ThatEventually[T any](t *testing.T, ctx context.Context, actual func() T, matcher Matcher[T], reasonFormat string, reasonArgs ...any) {
outer:
	for true {
		select {
		case <-ctx.Done():
			break outer
		default:
			if pass, _ := matcher(actual()); pass {
				break outer
			}
		}
		time.Sleep(10 * time.Millisecond)
	}
	That(t, actual(), matcher, reasonFormat, reasonArgs...)
}
