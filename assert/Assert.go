package assert

import (
	"testing"
)

func Fail(t *testing.T, format string, args ...any) {
	Logf(t, format, args...)
}

func Logf(t *testing.T, format string, args ...any) {
	t.Logf(format, args...)
	t.FailNow()
}

type Matcher[T any] func(actual T) (match bool, message string)
type MatcherImplementation[T any] func(actual T) (match bool, message string)
type MessageProvider[T any] func(actual T, extra string) string

// Helper functions
func CreateMatcher[T any](match MatcherImplementation[T], message MessageProvider[T]) Matcher[T] {
	return func(actual T) (bool, string) {
		if equal, msg := match(actual); equal {
			return true, ""
		} else {
			return false, message(actual, msg)

		}
	}
}
func CreateNotMatcher[T any](match MatcherImplementation[T], message MessageProvider[T]) Matcher[T] {
	return CreateMatcher(func(actual T) (bool, string) {
		equal, _ := match(actual)
		return !equal, ""
	}, message)
}
